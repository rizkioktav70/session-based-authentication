# Binar: Session Based Authentication

Di dalam repository ini terdapat implementasi session based authentication

## Getting Started

Untuk mulai membuat sebuah implementasi dari HTTP Server, mulainya menginspeksi file [`app/index.js`](./app/index.js), dan lihatlah salah satu contoh `controller` yang ada di [`app/controllers/mainController.js`](./app/controllers/mainController.js)

Lalu untuk menjalankan server, kalian tinggal jalanin salah satu script di package.json, yang namanya `start`.

```sh
npm run start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npx sequelize db:create` digunakan untuk membuat database
- `npx sequelize db:drop` digunakan untuk menghapus database
- `npx sequelize db:migrate` digunakan untuk menjalankan database migration
- `npx sequelize db:seed` digunakan untuk melakukan seeding
- `npx sequelize db:rollback` digunakan untuk membatalkan migrasi terakhir

# Output in Postman 

![screenshot](./screenshot/regis.png)
![screenshot](./screenshot/login.png)
![screenshot](./screenshot/eror.png)
![screenshot](./screenshot/success.png)

## Endpoints

Di dalam repository ini, terdapat dua endpoint utama, yaitu `login` dan `register`.

##### `/api/v1/login`

Endpoint ini digunakan untuk login

###### Request

```json
{
  "email":"bangoktav70@gmail.com",
  "password":"babangganteng"
}
```

###### Response

```json
{
  "id": 1,
  "email": "bangoktav70@gmail.com",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJiYW5nb2t0YXY3MEBnbWFpbC5jb20iLCJpYXQiOjE2NTIzMjQzMDl9.QDrHuj5DN21_DtvBf3NwNs4dLHu8kN2QHPmwGk6G1KQ",
  "createdAt": "2022-05-12T02:57:29.676Z",
  "updatedAt": "2022-05-12T02:57:29.676Z"
}
```

##### `/api/v1/register`

Endpoint ini digunakan untuk registrasi user

###### Request

```json
{
  "email":"bangoktav70@gmail.com",
  "password":"babangganteng"
}
```

###### Response

```json
{
  "id": 1,
  "email": "bangoktav70@gmail.com",
  "createdAt": "2022-05-12T02:57:29.676Z",
  "updatedAt": "2022-05-12T02:57:29.676Z"
}
```

## Pages

- [Home]((localhost:8000/))
- [Login](localhost:8000/login)
